/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.talend.jobs.bi;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.gwynniebee.talend.jobs.TalendJobResourceBase;
import com.gwynniebee.talend.jobs.bi.config.BIConfig;
import com.gwynniebee.talend.objects.TalendProjectJob;

/**
 * @author Prashant TR Rao
 * @author Anuj Aggarwal
 */
public class BIJob extends TalendJobResourceBase {
    /**
     * Pipeline Name.
     */
    private static final String PIPELINE_NAME = "daily";

    /**
     * Estimated time to complete.
     */
    private static final int ESTIMATED_COMPLETION_TIME = 14400;

    /**
     * Constructor.
     */
    public BIJob() {
        super();
        // Make a vector of Jobs
        Vector<String> t3reportingJobsVector = new Vector<String>();
        t3reportingJobsVector.add("dailyReport");
        t3reportingJobsVector.add("weeklyReport1");
        t3reportingJobsVector.add("weeklyReport2");
        // Add jobs vector to Talend Project.
        TalendProjectJob t3reportingProject =
                new TalendProjectJob("/home/gb/share/t3_inventoryManagementSystem", t3reportingJobsVector);
        // Make list of all Talend Projects.
        List<TalendProjectJob> talendProjectJobs = new ArrayList<TalendProjectJob>();
        talendProjectJobs.add(t3reportingProject);
        // Initialise using the list in previous step.
        this.setJobPipelineName(PIPELINE_NAME);
        this.init(BIConfig.getConfDir(),
                BIConfig.getJobName(), talendProjectJobs, ESTIMATED_COMPLETION_TIME);
    }
}
